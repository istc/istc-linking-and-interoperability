// cpp_program.cpp

#include <iostream>

#include "c_function.h"
#include "cpp_function.hh"
#include "MyCPPClass.hh"

// non-CPP functions must be called with C-style linkage
extern "C" 
{
  int c_function(int *);
  int fortran_function_(int *);
}

int main(){

  int val=1;
  std::cout << "Running CPP program with val = " << val << std::endl;
  
  // call c_function
  c_function(&val);
  std::cout << "c_function returns val = " << val << std::endl;

  // call fortran_function
  fortran_function_(&val);
  std::cout << "fortran_function returns val = " << val << std::endl;

  // call cpp function
  cpp_function(&val);
  std::cout << "cpp_function returns val = " << val << std::endl;

  // call cpp class method  
  MyCPPClass::MyCPPClassMethod(&val);
  std::cout << "MyCPPClass::MyCPPMethod() returns val = " << val << std::endl;

  std::cout << "CPP program complete." << std::endl;
  std::cout << std::endl;
  return 0;
}

