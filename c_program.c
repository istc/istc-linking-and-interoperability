// c_program.c

#include <stdio.h>
#include <stdlib.h>

#include "c_function.h"
#include "cpp_function.hh"
#include "cpp_class_method_wrapper.hh"

int main(int argc, char **argv){

  int val = strtol (argv[1], NULL, 10);
  printf("Running C program with val = %d\n", val);

  c_function(&val);
  printf("c_function returns with val = %d\n", val);

  double d_val = (double)val;
  c_function_d(&d_val);
  printf("c_function_d returns with val = %lf\n", d_val);
  
  fortran_function_(&val);
  printf("fortran_function returns with val = %d\n", val);

  cpp_function(&val);
  printf("cpp_function returns with val = %d\n", val);

  cpp_class_method_wrapper(&val);
  printf("cpp_class_method_wrapper returns val = %d\n", val);

  printf("C program complete.\n\n");
  return 0;
}
