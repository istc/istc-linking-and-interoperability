// cpp_function.hh

#ifdef __cplusplus
extern "C"
{
#endif
  int cpp_function(int *);
  int cpp_function_(int *);
#ifdef __cplusplus
}
#endif

