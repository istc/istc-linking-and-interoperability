// "c_function.h"

#ifdef __cplusplus
extern "C" {
#endif

int c_function(int *);

// fortran wrapper
int c_function_(int *);

#ifdef __cplusplus
}
#endif
